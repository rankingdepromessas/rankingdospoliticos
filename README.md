# RANKING DOS POLITICOS
Ouvimos muito em suas campanhas, vamos usar agora as nossas ferramentas pra cobra-los.

  - Muita internet;
  - Links para validar a informação;
  - Gráficos claros e goals pré definidos;

## O Projeto

### História
Ontem assistindo um vídeo de um candidato eleito para a Camara dos Deputados, fiquei embasbacado em ouvir tantos absurdos, tendo em vista que não simpatizo com o tal politico, logo pensei, por que não usar este material contra o próprio autor destes absurdos mas usando uma linguagem satírica, ácida, *LEGITIMA* e ao mesmo tempo contemporânea. Agradeço este insight a Childish Gambino, pois enquanto assistia ao vídeo do *Deputado Federal* tocava This is America. 

### Como 
A idéia em si é ter um local onde se escolhe um deputado, ou, senador, e atribui-se a ele uma meta. 
Por exemplo, usando meu case, o deputado em questão *PROMETE* algo que eu acho absurdo e impossível de concretizar. Porém essa ***coisa prometida*** pode sim ser extraída e elencada. Essa promessa se torna então uma ***meta***. 
Caso exista uma ***meta***, um **robô** irá diariamente disparar ***tweets*** e ***emails***, para o alvo *(deputado, senador e seus partidos - não integrantes, mas sim presidentes da sigla)*, *postará* imagens com textos em sua conta de *facebook* e *Instagram* e colocará a front ameaças feitas contra outros, linkando seus perfis sociais para um debate.
Tudo com seus devidos ***links***, para os usuários interagirem com a meta de forma interna à plataforma ou externa, propagando a meta por outros veículos de comunicação.



O critério de aceite da meta é ter um vídeo do próprio falando, um documento oficial extraído de sites do partido ou do proprietário da meta.




### - Sobre este repositório *(técnico)*

Estou abrindo a **TODOS** que tem interesse em colaborar com a idéia. A priori fica mais fácil elencar os principais atores que um projeto deste tipo demanda. 
***São eles:***


> Webdevelopers Front-end;

> Webdevelopers Back-end;

> Webdevelopers DevOps;

> Webdesigners;

> UX Developer/Designer;

Da parte de infraestrutura, vou apenas dar um norte pois acredito que este pode ser definido em consenso entre os agentes de produção.
***Aponto para:***

> VueJS;

> Laravel Lumen;

> Git;

> Material Design;

Todo o environment de back-end, como banco de dados, e onde e como hospedar a aplicação, deixo em aberto para quem for contribuir com o código.
Da parte de front-end, como sou desenvolvedor desta área, posso administrar assim que tiver um feed-back do back-end, pois em tese, podemos compartilhar métodos e praticas de DevOps.





**Sempre bom lembrar que floodar em comentários quais tecnologias ou métodos contribui menos do que fazer um fork no projeto e enviar um pull request com uma melhoria, portanto vamos focar na praticidade do desenvolvimento, em cima do todos os atores deste projeto, que pretendo desenvolver com um plural de ideologias e não apenas por um viés.**

